# README #

This is the Java version of the Nanoparticle charge transport project.

### Notes ###

* Full function of the Cython version
* Improved OOP desgin
* Efficient event updates


### Contribution guidelines ###
* Add GUI
* Add Grand Canonical Ensemble support
* Many more...

### Author ###
* Luman Qu, Marton Voros
* lmqu@ucdavis.edu